# Projet Final Terraform

## 1. Objectif

Vous devrez utiliser Terraform pour créer une instance de VM, et une base de données Mysql sur la région `Francfort 1` du cloud provider DigitalOcean. Il vous faudra installer et configurer le CMS Wordpres à l'aide de docker et Terraform. Vous devrez vous assurer que les ports `22`, `80` et `443` sont accessible sur votre instance. Par la suite, vous devrez faire pointer l'entrée DNS suivante "**pnom**.ca-ang.fabrique-it.fr" vers l'IP public de votre VM.

Le code source Terraform qui va vous permettre de réussir ce projet devra être déposé sur le repo suivant : [projet-terraform](https://gitlab.com/campus-academy-ang/software-deployment/projet-terraform/pnmom)

Si vous n'y avez pas accès (Benjamin Deletang et Guillaume Duarte), veuiller créer un autre repo sur Github ou Gitlab et m'envoyer le lien de vers votre repo sur ma boite mail.

> **_CRITERE D'EVALUATION:_**

1. Votre code devra être propre et commenté / 2 points
2. Une instance accessible via une IP publique / 3 points
3. Une base de données en service managé / 3 points
4. Installer wordpress via un script passé en user_data / 4 points
5. Ouvrir les ports `22`, `80` et `443` / 2 points
6. Une entrée dans le domaine ca-ang.fabrique-it.fr / 2 points
7. Une sortie permettant d'afficher votre IP public ou votre entrée DNS / 2 points
8. Un accès ssh à votre instance / 2 points
9. *optionnel* Utiliser des variables quand c'est possible / 1 point bonus
10. *optionnel* Activer le SSL pour communiquer sur le port `443` / 1 point bonus

## 2. Preparation

### 2.1. Gitlab - *optionnel*

Vous pouvez configurer Gitlab pour y [ajouter votre clé publique](https://gitlab.com/-/profile/keys) cela vous permettra de vous connecter au repository Gitlab en ssh afin de pousser vos sources.

### 2.2. Visual studio code - *optionnel*

#### 2.2.1. Pour les utilisateurs de WSL2

1. Installer l'extension "Remote - WSL"
2. Lancer votre terminal Linux
3. Lancer la commande `code .`, cela devrait vous permettre de lancer Visual Studio Code depuis un environnement Linux.

#### 2.2.2. Pour tous les utilisateurs

1. Cloner votre projet dans visual studio

```bash
git clone git@gitlab.com:campus-academy-ang/software-deployment/projet-terraform/pnmom.git
```

2. Créer les fichiers suivants :

* un fichier pour les variables si nécessaire
* un fichier `main.tf`

## 3. Terraform

> **_ATTENTION:_**  Lorque vous utilisez Terraform, n'oubliez pas de préfixer tous les noms de ressources par votre pnom, afin de ne pas construire une ressource qui aurait le même nom qu'un de vos camarades.

### 3.1. Installer le binaire Terraform sur Linux Debian/Ubuntu

```bash
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt update
sudo apt install terraform
```

### 3.2. Utiliser le provider de DigitalOcean

Voici votre bible sur tout ce qui concerne ce provider : [digitalocean](https://registry.terraform.io/providers/digitalocean/digitalocean/2.4.0/docs)

Voici des données spécifiques sur les [regions](https://developers.digitalocean.com/documentation/v2/#regions) et les [types d'instances](https://developers.digitalocean.com/documentation/v2/#sizes)

Il faut prendre la version, la 2.4.0 du provider.

> **_PRO TIPS:_**  cliquez sur le bouton 'USE PROVIDER' pour voir comment l'utiliser !

Configurer le token ci-dessous :

```terraform
provider "digitalocean" {
  token = "COLLER_VOTRE_TOKEN"
}
```

> **_IMPORTANT:_**  Le token sera désactivé à 17h, passé cette heure, vous ne pourrez plus accéder à digitalocean.

### 3.3. Créer une ressource pour la base de données managée par digitalocean

Vous devrez créer une base de données `MySQL` en `version 8` à l'aide d'une ressource
 de type base de données managée en vous basant sur la [documentation](https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs)

### 3.4. Créer et configurer l'instance

1. Vous devrez créer une instance de la plus petite taille possible sur la région de Frankfurt 1 avec la dernière version d'Ubuntu LTS disponible.

2. Vous devrez renseigner votre clé OpenSSH publique afin de pouvoir l'ajouter à l'instance, pour pouvoir vous y connecter ultérieurement.

3. Vous devrez définir une règle de firewall qui vous permettra d'ouvrir uniquement les ports `22`, `80` et `443` pour votre instance pour n'importe quelle adresse.

4. Vous devrez ajouter une entrée DNS au domaine `ca-ang.fabrique-it.fr` pour faire pointer l'IP public de votre instance vers le record suivant `pnom`. Pour cela vous pourrez utiliser la ressource suivante.

```Terraform
data "digitalocean_domain" "ca-anger" {
  name = "ca-ang.fabrique-it.fr"
}
```

> **_ATTENTION:_**  les ressources de type data doivent être préfixées par `data.` et donc être utilisées dans d'autre ressources de la façon suivante :

```terraform
data.digitalocean_domain.ca-anger.name
```

### 3.5. Créer une sortie (output)

Cette ouput vous permettra d'afficher votre IP public ou votre entrée DNS une la commande `terraform apply` terminée

### 3.6. Utilisation de Terraform

Pour utiliser les commandes ci-dessous, vous devrez être dans le même dossier que les fichiers `.tf` que vous aurez créés.

```bash
// initialiser les providers
Terraform init
// planifier les modifications
Terraform plan
// appliquer les modifications
Terraform apply
```

## 4. Wordpress

Il vous faudra installer Docker sur le serveur à l'aide de Terraform, des user_data de l'instance et d'un mécanisme de template. Le template devra utiliser un fichier `.sh` qui sera passé en user_data.
Voici des liens qui pourront vous aider à installer wordpress à l'aide de Terraform :

* [docker hub](https://hub.docker.com/_/wordpress)
* [user data](https://www.digitalocean.com/docs/droplets/how-to/provide-user-data/)
* [system de template](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file)

### 4.1. Installation

1. Installer la dernière version de docker :

```bash
curl -sL https://get.docker.com | sh
```

2. Lancer directement le conteneur Docker avec la commande suivante, en remplaçant les `xxxxxx` par les valeurs issues de Terraform et/ou d'un template

```bash
docker run -d -p 80:80 -v wordpress:/var/www/html -e WORDPRESS_DB_HOST=xxxxxx.ondigitalocean.com:25060 -e WORDPRESS_DB_USER=xxxxxx -e WORDPRESS_DB_PASSWORD=xxxxxx -e WORDPRESS_DB_NAME=xxxxxx --name wordpress wordpress:latest
```

## 5. Résulta attendu

> **_RECAPITULATIF:_**

1. créer une instance `Ubuntu 20.04` la plus petite en CPU et RAM possible sur la région `Francfort 1`
2. créer une base de données managée `mysql` en `version 8` sur la région `Francfort 1`
3. sécuriser votre instance avec des règles de firewall pour ouvrir uniquement les ports `22`, `80` et `443`
4. installer wordpress via le [user_data](https://www.digitalocean.com/docs/droplets/how-to/provide-user-data/)
5. créer une entrée DNS dans le domaine ca-ang.fabrique-it.fr vers votre IP public
6. créer une sortie (output) permettant d'afficher votre IP public ou votre entrée DNS (point 5.)
7. Créer un accès ssh à votre instance
8. *optionnel* utiliser des variables quand c'est possible
9. *optionnel* activer le SSL pour communiquer sur le port `443`

Votre site doit être accessible depuis votre URL : `http://pnom.ca-ang.fabrique-it.fr`
